/*
 * jresponse, JTAG interface for rESPonse hardware
 *
 * Copyright (C) 2022 Gary Wong
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef _JRESPONSE_H_
#define _JRESPONSE_H_

#define CHUNK_SIZE 0x400

#define PROTO_SYNC 0xC3
#define PROTO_SYNCACK 0xC4
#define PROTO_SYNCACKFLASH 0xC5
#define PROTO_TOKEN 0xFC
#define PROTO_DONE 0xFB
#define PROTO_ERR 0xFA

#endif
