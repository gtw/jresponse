/*
 * jresponse, JTAG interface for rESPonse hardware
 *
 * Copyright (C) 2022 Gary Wong
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <zlib.h>
#include "jresponse.h"

struct chunk {
    unsigned char dat[ CHUNK_SIZE ];
    int used;
    struct chunk *next;
};

static void usage( char *argv0 ) {
    
    fprintf( stderr, "Usage:\n\n    %s [-f] bistream tty-device\n"
	     "\n"
	     "Options:\n"
	     "  -f    Write bitstream to SPI flash\n", argv0 );
}

extern int main( int argc, char *argv[] ) {

    FILE *bit;
    z_stream zs;
    unsigned char in[ 0x10000 ];
    int jr;
    struct termios t;
    struct chunk first, *chunk;
    int state;
    unsigned char ack = PROTO_SYNCACK;
    int opt;
    
    while( ( opt = getopt( argc, argv, "fh" ) ) != -1 )
	switch( opt ) {
	case 'f':
	    ack = PROTO_SYNCACKFLASH;
	    break;
	    
	case 'h':
	    usage( argv[ 0 ] );
	    
	    return 0;
	    
	default:
	    usage( argv[ 0 ] );
	    
	    return 1;
	}
    
    if( argc != optind + 2 ) {
	usage( argv[ 0 ] );
	
	return 1;
    }

    if( !( bit = fopen( argv[ optind ], "rb" ) ) ) {
	perror( argv[ optind ] );

	return 1;
    }

    zs.zalloc = Z_NULL;
    zs.zfree = Z_NULL;
    zs.opaque = Z_NULL;
    if( deflateInit( &zs, Z_DEFAULT_COMPRESSION ) != Z_OK )
	return 1;    

    chunk = &first;
    chunk->used = 0;

    do {
	int flush;
	
	if( ( zs.avail_in = fread( in, 1, sizeof in, bit ) ) < 0 ) {
	    perror( argv[ optind ] );

	    return 1;
	}
	flush = !zs.avail_in;
	zs.next_in = in;

	do {
	    if( !( zs.avail_out = sizeof chunk->dat - chunk->used ) ) {
		chunk->next = malloc( sizeof *chunk->next );
		chunk = chunk->next;
		chunk->used = 0;
		zs.avail_out = sizeof chunk->dat;
	    }
	    zs.next_out = chunk->dat + chunk->used;

	    state = deflate( &zs, flush ? Z_FINISH : Z_NO_FLUSH );

	    chunk->used = zs.next_out - chunk->dat;
	} while( state != Z_STREAM_END && ( flush || zs.avail_in ) );
    } while( state != Z_STREAM_END );
    chunk->next = NULL;
    
    printf( "Uncompressed %ld, compressed %ld\n", zs.total_in, zs.total_out );
    fclose( bit );
    chunk = &first;

    if( ( jr = open( argv[ optind + 1 ], O_RDWR ) ) < 0 ) {
	perror( argv[ optind + 1 ] );

	return 1;
    }

    tcgetattr( jr, &t );
    cfmakeraw( &t );
    tcsetattr( jr, TCSANOW, &t );
    
    while( 1 ) {
	int n;
	unsigned char c;

	n = read( jr, &c, 1 );

	if( n < 1 ) {
	    perror( argv[ optind ] );

	    return 1;
	} else if( c == PROTO_SYNC )
	    break;
	else
	    putchar( c );
    }

    write( jr, &ack, 1 );
    if( ack == PROTO_SYNCACKFLASH ) {
	unsigned char eraseblocks = ( zs.total_in + 0xFFFF ) >> 16;
	write( jr, &eraseblocks, 1 );
    }
    
    puts( "Synchronised" );
    
    while( 1 ) {
	int n;
	unsigned char c;

	n = read( jr, &c, 1 );

	if( n < 1 ) {
	    perror( argv[ optind ] );

	    return 1;
	}
	
	switch( c ) {
	case PROTO_SYNC:
	    break;

	case PROTO_TOKEN:
	    if( chunk ) {
		static int toggle;
		unsigned char *p = chunk->dat;

		while( p < chunk->dat + chunk->used ) {
		    int n = write( jr, p, chunk->dat + chunk->used - p );

		    if( n < 0 ) {
			perror( argv[ optind + 1 ] );

			return 1;
		    } else		    
			p += n;
		}
		
		chunk = chunk->next;
		putchar( toggle ? '/' : '\\' );
		toggle = !toggle;
	    } else
		putchar( '*' );
	    putchar( '\b' );
	    fflush( stdout );
	    break;

	case PROTO_DONE:
	    return 0;

	case PROTO_ERR:
	    return -1;
	    
	default:
	    putchar( c );
	}
    }
}
