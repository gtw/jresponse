/*
 * jresponse, JTAG interface for rESPonse hardware
 *
 * Copyright (C) 2022 Gary Wong
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <driver/gpio.h>
#include <driver/usb_serial_jtag.h>
#include <esp_vfs_dev.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <rom/miniz.h>
#include <soc/gpio_struct.h>
#include <time.h>
#include <unistd.h>
#include "jresponse.h"

#define GPIO_TCK 2
#define GPIO_TDI 5
#define GPIO_TDO 7
#define GPIO_TMS 9

#define RX_BUF_SIZE 0x20000

static void sdr( int bit ) {

    if( bit ) {
	GPIO.out_w1tc.out_w1tc = ( 1 << GPIO_TCK ) | ( 1 << GPIO_TMS );
	GPIO.out_w1ts.out_w1ts = 1 << GPIO_TDI;
	GPIO.out_w1ts.out_w1ts = 1 << GPIO_TCK;
    } else {
	GPIO.out_w1tc.out_w1tc = ( 1 << GPIO_TCK ) | ( 1 << GPIO_TMS ) |
	    ( 1 << GPIO_TDI );
	GPIO.out_w1ts.out_w1ts = 1 << GPIO_TCK;
    }
}

static void sdrm( int bit ) {

    if( bit ) {
	GPIO.out_w1tc.out_w1tc = 1 << GPIO_TCK;
	GPIO.out_w1ts.out_w1ts = ( 1 << GPIO_TDI ) | ( 1 << GPIO_TMS );
	GPIO.out_w1ts.out_w1ts = 1 << GPIO_TCK;
    } else {
	GPIO.out_w1tc.out_w1tc = ( 1 << GPIO_TCK ) | ( 1 << GPIO_TDI );
	GPIO.out_w1ts.out_w1ts = ( 1 << GPIO_TMS );
	GPIO.out_w1ts.out_w1ts = 1 << GPIO_TCK;
    }
}

static void tx( unsigned val, int len ) {

    while( len > 1 ) {
	sdr( val & 1 );
	val >>= 1;
	len--;
    }

    sdrm( val & 1 );
}

// Shift into instruction register (starting and ending in idle state).
static void sir( unsigned char opcode ) {
    
    // Shift IR
    sdrm( 0 );
    sdrm( 0 );
    sdr( 0 );
    sdr( 0 );

    tx( opcode, 8 );
	
    // Return to idle
    sdrm( 0 );
    sdr( 0 );
}

static void tdi_start( int resume ) {
    
    // Shift DR
    sdrm( 0 );
    sdr( 0 );
    
    if( !resume )
	sdr( 0 );
}

static void tdi_continue( unsigned val, int len ) {
    
    while( len ) {
	sdr( val & 1 );
	val >>= 1;
	len--;
    }
}

static void tdi_continue_rev( unsigned val, int len ) {

    unsigned mask = 1 << ( len - 1 );
    
    while( len ) {
	sdr( val & mask );
	val <<= 1;
	len--;
    }
}

static void tdi_end( unsigned val, int len, int pause ) {

    tx( val, len );

    // Return to idle
    if( !pause )
	sdrm( 0 );
    
    sdr( 0 );
}

static void tdi_end_rev( unsigned val, int len, int pause ) {

    unsigned mask = 1 << ( len - 1 );
    
    while( len > 1 ) {
	sdr( val & mask );
	val <<= 1;
	len--;
    }

    sdrm( val & mask );

    // Return to idle
    if( !pause )
	sdrm( 0 );
    
    sdr( 0 );
}

// Shift data in (starting and ending in idle or pause DR state).
static void tdi( unsigned val, int len, int resume, int pause ) {

    tdi_start( resume );
    tdi_end( val, len, pause );
}

static unsigned tdo_end( int len ) {

    unsigned val = 0;
    int i;

    for( i = 0; i < len - 1; i++ ) {
	GPIO.out_w1tc.out_w1tc = ( 1 << GPIO_TCK ) | ( 1 << GPIO_TDI ) |
	    ( 1 << GPIO_TMS );
	GPIO.out_w1ts.out_w1ts = 1 << GPIO_TCK;
	
	if( GPIO.in.data & ( 1 << GPIO_TDO ) )
	    val |= 1 << i;
    }
    
    GPIO.out_w1tc.out_w1tc = ( 1 << GPIO_TCK ) | ( 1 << GPIO_TDI );
    GPIO.out_w1ts.out_w1ts = 1 << GPIO_TMS;
    GPIO.out_w1ts.out_w1ts = 1 << GPIO_TCK;
    
    if( GPIO.in.data & ( 1 << GPIO_TDO ) )
	val |= 1 << i;

    // Return to idle
    sdrm( 0 );
    sdr( 0 );
    
    return val;
}

// Shift data out (starting and ending in idle state).
static unsigned tdo( int len ) {

    unsigned val;
    
    // Shift DR
    sdrm( 0 );
    sdr( 0 );
    sdr( 0 );

    return tdo_end( len );
}

static void tick( int n ) {

    while( n-- ) {
	GPIO.out_w1tc.out_w1tc = ( 1 << GPIO_TCK ) | ( 1 << GPIO_TMS ) |
	    ( 1 << GPIO_TDI );
	GPIO.out_w1ts.out_w1ts = 1 << GPIO_TCK;
    }
}

static void show_status( unsigned st ) {

    if( st & 0x1 )
	puts( "Transparent mode" );
    if( ( st & 0xE ) == 0 )
	puts( "Target SRAM" );
    else if( ( st & 0xE ) == 1 << 1 )
	puts( "Target Efuse" );
    else
	puts( "Target unknown" );
    if( st & 0x10 )
	puts( "JTAG active" );
    if( st & 0x20 )
	puts( "PWD prot" );
    if( st & 0x80 )
	puts( "Decrypt ena" );
    if( st & 0x100 )
	puts( "DONE" );
    if( st & 0x200 )
	puts( "ISC Ena" );
    if( st & 0x400 )
	puts( "Write ena" );
    if( st & 0x800 )
	puts( "Read ena" );
    if( st & 0x1000 )
	puts( "Busy" );
    if( st & 0x2000 )
	puts( "Fail" );
    if( st & 0x4000 )
	puts( "FEA OTP" );
    if( st & 0x8000 )
	puts( "Decrypt only" );
    if( st & 0x10000 )
	puts( "PWD ena" );
    if( st & 0x100000 )
	puts( "Encrypt pre" );
    if( st & 0x200000 )
	puts( "Std pre" );
    if( st & 0x400000 )
	puts( "SPIm fail 1" );
    if( ( st & 0x3800000 ) == 0 )
	puts( "No error" );
    else if( ( st & 0x3800000 ) == 1 << 23 )
	puts( "ID error" );
    else if( ( st & 0x3800000 ) == 2 << 23 )
	puts( "CMD error" );
    else if( ( st & 0x3800000 ) == 3 << 23 )
	puts( "CRC error" );
    else if( ( st & 0x3800000 ) == 4 << 23 )
	puts( "PRMB error" );
    else if( ( st & 0x3800000 ) == 5 << 23 )
	puts( "ABRT error" );
    else if( ( st & 0x3800000 ) == 6 << 23 )
	puts( "OVFL error" );
    else if( ( st & 0x3800000 ) == 7 << 23 )
	puts( "SDM error" );
    if( st & 0x4000000 )
	puts( "Exec err" );
    if( st & 0x8000000 )
	puts( "ID err" );
    if( st & 0x10000000 )
	puts( "Inv cmd" );
    if( st & 0x20000000 )
	puts( "SED err" );
    if( st & 0x40000000 )
	puts( "Bypass" );
    if( st & 0x80000000 )
	puts( "Flow through" );    
}

static int chk_stat( unsigned mask, unsigned check, int timeout ) {

    unsigned st;

    while( 1 ) {
	sir( 0x3C );
	st = tdo( 32 );
	
	if( ( st & mask ) == check )
	    return 0;
	else if( timeout < 1 )
	    break;
	
	tick( 0x800 );
	timeout--;
    }

    printf( "Unexpected status %08X\n", st );
    show_status( st );
    
    return -1;
}

static void transfer( void ) {
    
    static tinfl_decompressor tinfl;
    static unsigned char inflbuf[ TINFL_LZ_DICT_SIZE ], usbbuf[ CHUNK_SIZE ];
    unsigned char *outp = inflbuf;
    int outn = sizeof inflbuf;    
    int ack = 0;
    int rbytes = 0, wbytes = 0;
    tinfl_status st = -1;
    int i;
    unsigned id;
    int flash;
    
    // Sync with host
    while( 1 ) {
	unsigned char c = 0;

	if( usb_serial_jtag_read_bytes( &c, 1, 0x20 ) == 1 ) {
	    if( c == PROTO_SYNCACK ) {
		flash = 0;
		break;
	    } else if( c == PROTO_SYNCACKFLASH ) {
		c = 0;
		usb_serial_jtag_read_bytes( &c, 1, 0x100 );
		flash = c;
		break;
	    }
	} else {
	    putchar( PROTO_SYNC );
	    fflush( stdout );
	    fsync( STDOUT_FILENO );
	}
    }

    tinfl_init( &tinfl );

    for( i = 0; i < RX_BUF_SIZE; i += CHUNK_SIZE )
	putchar( PROTO_TOKEN );
    
    fflush( stdout );
    fsync( STDOUT_FILENO );
    
    // Reset
    for( i = 0; i < 5; i++ )
	sdrm( 0 );

    // Idle
    sdr( 0 );

    // READ_ID (E0)
    sir( 0xE0 );
    id = tdo( 32 );
    printf( "Read ID %08X\n", id );

    // LSC_PRELOAD (1C)
    sir( 0x1C );
    tdi_start( 0 );
    for( i = 0; i < 15; i++ )
	tdi_continue( 0xFFFFFFFF, 32 );    
    tdi_end( 0x3FFFFFFF, 30, 0 );

    // ISC_ENABLE (C6)
    sir( 0xC6 );
    tdi( 0, 8, 0, 0 );
    tick( 0x10 );
    
    chk_stat( 0x0000B61E, 0x00000610, 0x10 ); // write enabled, no fail, no busy
    
    // ISC_ERASE (0E)
    sir( 0x0E );
    tdi( 0x01, 8, 0, 0 );

    if( flash ) {
	// Reset
	for( i = 0; i < 5; i++ )
	    sdrm( 0 );

	// Idle
	sdr( 0 );

	// ISC_NOOP
	sir( 0xFF );

	tick( 0x02 );

	// LSC_PROG_SPI
	sir( 0x3A );
	tdi( 0x68FE, 16, 0, 0 );
	
	tick( 0x20 );

	for( i = 0; i < flash; i++ ) {
	    printf( "Erasing block %d/%d...", i, flash );
	    fflush( stdout );
	    
	    tdi( 0x60, 8, 0, 1 ); // (06) Write Enable

	    tdi_start( 1 );
	    tdi_continue_rev( 0xD8, 8 ); // Block Erase
	    tdi_end_rev( i << 16, 24, 0 );

	    while( 1 ) {
		int x;
		
		tdi_start( 0 );
		tdi_continue_rev( 0x05, 8 ); // Read Status Register 1

		x = tdo_end( 8 );
		
		if( !( x & 0xC1 ) )
		    break;
		
		vTaskDelay( 1 );
	    }

	    puts( " done." );
	}
    } else {    
	// LSC_INIT_ADDRESS (46)
	sir( 0x46 );
	tdi( 0x01, 8, 0, 0 );

	// LSC_BITSTREAM_BURST (7A)
	sir( 0x7A );
	tdi_start( 0 );
    }
    
    while( st != TINFL_STATUS_DONE ) {
	static int shown;
	unsigned char *p = usbbuf;
	int n = usb_serial_jtag_read_bytes( usbbuf, sizeof usbbuf, 0 );
	int t;

	t = time( NULL );
	if( t > shown ) {
	    printf( "%d read, %d written\n", rbytes, wbytes );
	    shown = t;
	    vTaskDelay( 1 );
	}

	if( ( ack += n ) >= CHUNK_SIZE ) {
	    putchar( PROTO_TOKEN );
	    fflush( stdout );
	    fsync( STDOUT_FILENO );
	    ack -= CHUNK_SIZE;
	}

	rbytes += n;
	
	while( n ) {
	    size_t inbytes = n, outbytes = outn;
	    
	    st = tinfl_decompress( &tinfl, p, &inbytes, inflbuf, outp, &outbytes,
				   TINFL_FLAG_HAS_MORE_INPUT |
				   TINFL_FLAG_PARSE_ZLIB_HEADER );

	    p += inbytes;
	    n -= inbytes;

	    outn -= outbytes;

	    while( outbytes ) {
		int endwrite;
		
		if( flash && !( wbytes & 0xFF ) ) { // starting new page
		    tdi( 0x60, 8, 0, 1 ); // (06) Write Enable

		    tdi_start( 1 );
		    tdi_continue_rev( 0x02, 8 ); // Page Program
		    tdi_continue_rev( wbytes, 24 );
		}

		endwrite = ( st == TINFL_STATUS_DONE && outbytes == 1 ) ||
		    ( flash && ( ( wbytes & 0xFF ) == 0xFF ) );
		if( endwrite )
		    tdi_end_rev( *outp, 8, 0 );
		else
		    tdi_continue_rev( *outp, 8 );

		outp++;
		outbytes--;
		wbytes++;
		
		if( flash && endwrite )
		    while( 1 ) {
			int x;
		
			tdi_start( 0 );
			tdi_continue_rev( 0x05, 8 ); // Read Status Register 1

			x = tdo_end( 8 );
		    
			if( !( x & 0xC1 ) )
			    break;
		    }
	    }
	    
	    if( !outn ) {
		outp = inflbuf;
		outn = sizeof inflbuf;
	    }
	}
    }

    // Idle
    sdrm( 0 );
    sdrm( 0 );
    sdr( 0 );

    printf( "%d read, %d written\n", rbytes, wbytes );
        
    chk_stat( flash ? 0x03802000 : 0x03802100,
	      flash ? 0x00000000 : 0x00000100, 0x10 ); // done, no fail
    
    // ISC_NOOP (FF)
    sir( 0xFF );
    
    // USERCODE (C0)
    sir( 0xC0 );
    id = tdo( 32 );
    printf( "Read user code %08X\n", id );

    // ISC_DISABLE (26)
    sir( 0x26 );
    tick( 0x10 );
    
    // ISC_NOOP (FF)
    sir( 0xFF );

    if( flash ) {
	sir( 0x79 ); // LSC_REFRESH

	tick( 0x1000 );
    }
    
    putchar( chk_stat( flash ? 0x03802000 : 0x03802100,
		       flash ? 0x00000000 : 0x00000100, // done, no fail
		       0x10 ) ? PROTO_ERR : PROTO_DONE );
    fflush( stdout );
    fsync( STDOUT_FILENO );
    
    // Reset
    for( i = 0; i < 5; i++ )
	sdrm( 0 );

    // Idle
    sdr( 0 );
}

extern void app_main( void ) {

    usb_serial_jtag_driver_config_t cfg = {
	.rx_buffer_size = RX_BUF_SIZE,
	.tx_buffer_size = 0x40
    };
    usb_serial_jtag_driver_install( &cfg );

    esp_vfs_usb_serial_jtag_use_driver();
    
    puts( "Initialising..." );

    static gpio_config_t jtag_out = {
	.mode = GPIO_MODE_OUTPUT,
	.pin_bit_mask = ( 1 << GPIO_TDI ) | ( 1 << GPIO_TCK ) | ( 1 << GPIO_TMS )
    };

    gpio_config( &jtag_out );
    
    GPIO.out_w1tc.out_w1tc = ( 1 << GPIO_TCK ) | ( 1 << GPIO_TDI ) |
	( 1 << GPIO_TMS );
    
    static gpio_config_t jtag_in = {
	.mode = GPIO_MODE_INPUT,
	.pin_bit_mask = 1 << GPIO_TDO,
	.pull_down_en = GPIO_PULLDOWN_ENABLE
    };

    gpio_config( &jtag_in );

    while( 1 )
	transfer();
}
